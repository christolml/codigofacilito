/* video de codigo facilito de youtube, GOROUTINES */
package main

import (
	"fmt"
	"strings"
	"time"
)

func main() {
	// mi_nombre_lento("Christopher:1232:metodo32:paco")
	go mi_nombre_lento("Christopher")
	fmt.Println("Esta linea esta despues de ejecutar el metodo de mi_nombre_lento")
	var wait string
	fmt.Scanln(&wait)

}

// se va  adescomponer el nombre con las letras que lo componee, por medio del split que esta en la libreria de strings
func mi_nombre_lento(name string) {
	/* 	en el parentesis va el caracter con el que vamos a separar nuestro string, cuando se no se le indica el
	caracter y se pasa asi blanco, nos va separar nuestro string letra por letra */
	letras := strings.Split(name, "") // ":"

	for _, letra := range letras {
		time.Sleep(1000 * time.Millisecond) //por cada que imprima la letra se espera un segundo
		fmt.Println(letra)
	}

}
