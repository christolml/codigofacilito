package main

import (
	"encoding/json"
	"net/http"
)

// en esta estructura los atributos deben comenzar en mayuscula para que nos permita exportarlo en json, pero si queremos que en nuestro json
// se muestre en minuscula debemos hacer lo que esta a un lado de los atributos, con eso le estamos dando un nombre que le queremos dar a ese atributo y se mostrará
type Curso struct {
	Title     string `json:"title"`
	NumVideos int
}

type Cursos []Curso

// en la variable curso que es de la estructura Curso se transformo a json
func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		cursos := Cursos{
			Curso{"Curso de GO", 30},
			Curso{"Curso de C", 30},
			Curso{"Curso de C++", 30},
			Curso{"Curso de C#", 30},
			Curso{"Curso de Node.js", 30},
		}
		json.NewEncoder(w).Encode(cursos) //a NewEncoder se le tiene que pasar un string de datos como el ResponseWriter
	})

	http.ListenAndServe(":8080", nil)
}
