package main

import (
	"bufio"
	"fmt"
	"os"
)

// con panic me paniquea las demas funciones y solo devuelve hasta el punto del panic, con recover es una forma de detener el paniqueo

func main() {
	executeReadFile()
	fmt.Println("Nunca me voy a imprimmir")

}

func executeReadFile() {
	ejecucion := readFile()
	fmt.Println(ejecucion)
}

func readFile() bool {
	archivo, err := os.Open("./infe.txt") //se abre el archivo
	defer func() {                        //con defer nos estamos asegurando que se ejecute ese codigo sin importar que pase en lo demas del main
		archivo.Close()
		fmt.Println("Defer")

		recover()
	}()

	if err != nil {
		panic(err)
	}

	scanner := bufio.NewScanner(archivo) //le pasamos el archivo con que vamos a trabajar a nuestro scanener
	var i int
	for scanner.Scan() { //se itera las lineas del archivo, con Scan se va leyendo las lineas
		i++
		linea := scanner.Text() //este nos regresa lo que tenemos en la linea
		fmt.Println(i)
		fmt.Println(linea)
	}

	fmt.Println("Nunca me ejecute")
	return true

}
