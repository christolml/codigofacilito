package main

import (
	"fmt"
	"io/ioutil"
)

// la forma en la que se trabajo el archivo se manda a memoria y luego lo leemos, de esta forma es algo muy lento

func main() {
	text, err := ioutil.ReadFile("./text.txt") //ReadFile nos devuelve un error y el contenido ya leido
	if err != nil {
		panic("Hubo un error")
	}

	fmt.Println(string(text))
}
