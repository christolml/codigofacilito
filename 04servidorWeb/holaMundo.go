package main

import (
	"io"
	"net/http"
)

func main() {

	http.HandleFunc("/holamundo", func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, "Holaaaaaaaa")
	})

	http.HandleFunc("/", handler)

	http.ListenAndServe(":8080", nil)

}

/* ResponseWriter es una estructura nos permite definir como vamos a responder la peticion,
*http.Request es un puntero a la informacion de la informacion, es decir, que nos envio el nacegador */
func handler(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Holi mundo")

}
