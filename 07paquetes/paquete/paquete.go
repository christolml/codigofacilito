package paquete

var NombreDistinto string

/* init preconfigura el paquete, cuando se importa un paquete	la funcion que se ejecuta inicialmente es el init, en este caso en edTeam01-03.go
esta importando este paquete */
func init() {
	NombreDistinto = "Que onda lml"
}

func HolaMundo() string {
	// return "Holi mundo"
	return NombreDistinto
}
