package main

import (
	"fmt"
)

// ----------------- CANALES   nos permite que nuestras go routines se esten comunicando entre ellas(se esten pasando informacion)
func main() {

	channel := make(chan string) //se define el canal y el tipo de datos que se va a enviar por el canal

	/* 	esta go routine se pasa el canal para que pueda tener acceso al mismo y pueda enviar informacion a el
	   	la firma de la func esta recibindo un channel y el pasa chan string como cuando se definio el canal */
	go func(channel chan string) {
		for {
			var name string
			fmt.Scanln(&name)
			channel <- name //se envia el name al canal
		}

	}(channel)

	for {
		msg := <-channel // se esta recibiendo la informacion del canal
		fmt.Println("El canal recibio:", msg)
	}

}
